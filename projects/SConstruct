"""
SCons entry point

Invoke the command "scons" with a shell while the current directory is this file's directory
"""

import os

__version__ = "1.0.0"


"""
Helper functions
"""
def has_subsidary_scons(node):
    """
    Check if a node has a subsidary SConscript file exactly named "SConscript"
    :param node: A file or directory node; string are also accepted (Dir, File, str)
    :return: Boolean indicating that the provided node (namely directory node) contains an SConscript file (bool)
    """
    try:
        node = Dir(node)
    except TypeError:
        return False

    filenames = os.listdir(node.abspath)
    for filename in filenames:
        if filename == "SConscript":
            ret = True
            break
    else:
        ret = False
    return ret


"""
CLI arguments
"""
AddOption("--proj", "--project",
          type="string",
          metavar="<directory name>",
          default="lpc1758_freertos",
          help="Specify a target project directory to build.")

target_project_dirname = GetOption("proj")


"""
Register other subsidary SConscripts
"""
SConscript("scons_arm")


"""
Jump to the target project subsidary SConscript
"""
if target_project_dirname is None:
    raise AssertionError("Target project directory is not defined! Use the option --proj <project directory>")

if not os.path.isdir(target_project_dirname) or not has_subsidary_scons(target_project_dirname):
    raise AssertionError("The provided target project directory [{}] is invalid!".format(target_project_dirname))

SConscript(os.path.join(Dir(".").abspath, target_project_dirname, "SConscript"))
